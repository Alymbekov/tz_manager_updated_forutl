from django.contrib import admin
from .models import Task, Tag


class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'description', 'status', 'created_at', 'finished_date']


class TagAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'date']

admin.site.register(Task, TaskAdmin)
admin.site.register(Tag, TagAdmin)