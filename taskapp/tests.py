# from django.test import TestCase
# from django.contrib.auth.models import User
# from .models import Task, Tag


# class TaskTests(TestCase):
    
#     @classmethod
#     def setUpTestData(cls):
#             # Create a user
#         testuser1 = User.objects.create_user(
#             username='testuser1', password='abc123')
#         testuser1.save()
#         # Create a blog task
#         test_task = Task.objects.create(
#             author=testuser1, title='Task title', description='Description content...')
#         test_task.save()


#     def test_task_content(self):
#         task = Task.objects.get(id=1)
#         expected_author = f'{task.author}'
#         expected_title = f'{task.title}'
#         expected_description = f'{task.description}'
#         self.assertEqual(expected_author, 'testuser1')
#         self.assertEqual(expected_title, 'Task title')
#         self.assertEqual(expected_description, 'Description content...')


# class TagTests(TestCase):
    
#     @classmethod
#     def setUpTestData(cls):
#             # Create a user
#         testuser1 = User.objects.create_user(
#             username='testuser1', password='abc123')
#         testuser1.save()
#         # Create a blog tag
#         test_tag = Tag.objects.create(
#             author=testuser1, title='Tag title')
#         test_tag.save()


#     def test_tag_content(self):
#         tag = Tag.objects.get(id=1)
#         expected_author = f'{tag.author}'
#         expected_title = f'{tag.title}'
#         self.assertEqual(expected_author, 'testuser1')
#         self.assertEqual(expected_title, 'Tag title')
