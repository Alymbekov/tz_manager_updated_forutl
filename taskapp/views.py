from rest_framework import viewsets
# from django.contrib.auth import get_user_model

from .models import Tag, Task
from .permissions import IsAuthorOrReadOnly
from .serializers import TagSerializer, TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    lookup_field = 'id'
    queryset = Task.objects.all()
    permission_classes = (IsAuthorOrReadOnly,)


class TagViewSet(viewsets.ModelViewSet):
    serializer_class = TagSerializer
    lookup_field = 'id'
    queryset = Tag.objects.all()
    permission_classes = (IsAuthorOrReadOnly,)


