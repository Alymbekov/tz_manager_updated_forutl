# Generated by Django 2.2 on 2020-03-24 06:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.IntegerField(choices=[(2, 'В ожидании'), (3, 'Законченный'), (1, 'В процессе')], default=2, verbose_name='Статус'),
        ),
    ]
